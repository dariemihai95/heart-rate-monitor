# -*- coding: utf-8 -*-
"""
Created on Sun Apr 29 11:30:22 2018
    
@author: Mihai Darie
"""

import time
import scipy.signal
import numpy as np
import cv2 
import matplotlib.pyplot as plt
import sys
sys.path.insert(1, r'./../functions')  # add to pythonpath
from detect_peaks import detect_peaks
#from savitzky_golay import savitzky_golay



#connecting with the captured video file taken from mobile
cap = cv2.VideoCapture('cam_samples/20180530_214731.mp4') #open cv to take the video

fps = cap.get(cv2.CAP_PROP_FPS)
print ("This video has:",np.round_(fps, decimals=1), "FPS")

#getting the number of total frames 
no_of_frames = int(cap.get(7))

#assigning an initial zero red value for every frame
red_plane = np.zeros(no_of_frames)

#time_list is used for storing occurence time of each frame in the video  
time_list=[]
t=0

#camera frame per second is 60 and so each frame acccurs after 1/60th second
difference = 1/fps
for i in range(no_of_frames):

    #reading the frame
    ret,frame = cap.read()
    length,width,channels = frame.shape

    #calculating average red value in the frame
    red_plane[i] = np.sum(frame[:,:,2])/(length*width)
    time_list.append(t)
    t = t+ difference

print("It's length is:",np.round_(t, decimals=1),"seconds" )
cap.release()

#finding the middle of the blood flow variation, so we set a minimum treshold
#avg=min(red_plane)+((max(red_plane)-min(red_plane))/2)
#print(avg)
#different aproach, and I don't need this anymore


#smoothing the signal using Savitzky Golay
red_plane_smooth = scipy.signal.savgol_filter(red_plane, 31, 3)



#this doesn't work, obsolete unfortunantely
#y_sg = scipy.signal.savgol_filter(y_noise, 51, 3)

#self explanatory, algorithm to detect peaks
HR_signal = detect_peaks(red_plane_smooth, mph=min(red_plane), mpd=6)
#minimum peak distance (how many numbers to jump) cannot be less than 6 because in 60frames video it will be 180 bpm

#for i in range (len(HR_signal)):
#    instant_HR = (HR_signal[i+1]-HR_signal[i])
#    print("Your instant HR is"+instant_HR)


#this is only for showing the no. of indexes of the peaks(to stop the following for loop)
total_peaks = HR_signal.tolist().index(max(HR_signal))

HR_data = np.zeros(total_peaks)

for i in range(total_peaks):
    time_diff = time_list[HR_signal[i+1]]-time_list[HR_signal[i]]
    instant_HR = round(60/time_diff)
    print('Instant_HR:',instant_HR)
    HR_data[i] = np.array(instant_HR)
    time.sleep(0.5)

#plotting the blood impulses in regards of time
time.sleep(0.5)

plt.figure(figsize=(30,5))
plt.plot(time_list, red_plane)
plt.ylabel('Blood Flow')
plt.xlabel('time - seconds')
plt.plot(time_list, red_plane_smooth, '-rx', markevery=HR_signal)
plt.show()


#print(red_plane[HR_signal])
#here i am taking all the values included in the time_list from the lowest index to the highest
time_diff = time_list[HR_signal[total_peaks]]-time_list[HR_signal[0]]


HR = round((len(HR_signal)/time_diff)*60)

time.sleep(1)

print('You have', HR, 'BPM')

np.savetxt('Data/CP10.csv',np.column_stack([HR_data]),fmt = '%.0f')

#plt.plot(time_list(HR_signal), HR_data, '-gx', markevery=HR_signal)
#plt.show()